/* Copyright 2019, 2020 Clément Saccoccio */

/*
	This file is part of Kingdom Blazon Generator.

	Kingdom Blazon Generator is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	any later version.

	Kingdom Blazon Generator is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Kingdom Blazon Generator.  If not, see <https://www.gnu.org/licenses/>.
*/

export interface AnyObject {
	[key: string]: any,
}

export function downloadImage(url: string): Promise<HTMLImageElement> {
	return new Promise((resolve, reject) => {
		const img = new Image();
		img.addEventListener('load', () => {
			resolve(img);
		});
		img.addEventListener('error', reject);
		img.src = url;
	});
}

export function downloadJson(url: string): Promise<AnyObject> {

	return new Promise((resolve, reject) => {

		const xhr = new XMLHttpRequest();

		xhr.addEventListener('load', () => {
			try {
				resolve(JSON.parse(xhr.responseText));
			} catch (e) {
				reject(e);
			}
		})

		xhr.addEventListener('error', reject);
		xhr.addEventListener('timeout', reject);
		xhr.addEventListener('abort', reject);

		xhr.open('GET', url);
		xhr.send();
	});
}

export function setState(element: HTMLElement, active: boolean) {
	if (active) {
		element.classList.add('on');
		element.classList.remove('off');
	} else {
		element.classList.remove('on');
		element.classList.add('off');
	}
}
