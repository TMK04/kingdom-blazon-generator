/* Copyright 2019, 2020 Clément Saccoccio */

/*
	This file is part of Kingdom Blazon Generator.

	Kingdom Blazon Generator is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	any later version.

	Kingdom Blazon Generator is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Kingdom Blazon Generator.  If not, see <https://www.gnu.org/licenses/>.
*/

import { setState } from './Utils';
import Game from './Game';

const gamesBtns = document.getElementsByClassName('game-logo') as HTMLCollectionOf<HTMLElement>;
const leftBtn = document.getElementById('left-btn');
const rightBtn = document.getElementById('right-btn');
const listeners: SelectedGameChangedListener[] = [];
let selectedGame = Game.TWO_CROWNS;

leftBtn.addEventListener('click', () => {
	if (selectedGame.hasPrevious()){
		selectedGame = selectedGame.previous;
		refresh(selectedGame.next);
	}
});

rightBtn.addEventListener('click', () => {
	if (selectedGame.hasNext()){
		selectedGame = selectedGame.next;
		refresh(selectedGame.previous);
	}
});

refresh();

function refresh(previousSelectedGame?: Game) {

	for (let i=0 ; i<gamesBtns.length ; ++i){
		gamesBtns[i].style.display = i === selectedGame.index ? 'block' : 'none';
	}

	setState(leftBtn, selectedGame.hasPrevious());
	setState(rightBtn, selectedGame.hasNext());

	if (previousSelectedGame) {
		for (const listener of listeners){
			listener(selectedGame, previousSelectedGame);
		}
	}
}

export type SelectedGameChangedListener = (selectedGame: Game, previousSelectedGame: Game) => void;

export function onSelectedGameChanged(listener: SelectedGameChangedListener) {
	listeners.push(listener);
}

export function getSelectedGame(): Game {
	return selectedGame;
}
