/* Copyright 2019, 2020 Clément Saccoccio */

/*
	This file is part of Kingdom Blazon Generator.

	Kingdom Blazon Generator is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	any later version.

	Kingdom Blazon Generator is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Kingdom Blazon Generator.  If not, see <https://www.gnu.org/licenses/>.
*/

import { downloadImage } from './Utils';
import Color from './Color';
import { setImageSmoothing } from './Polyfills'

/**
 * Sprite.getPixelColor can be very low efficient if called repeatedly.
 * Instead, use Raster.getPixelColor.
 */
class Raster {

	private width: number;
	private data: Uint8ClampedArray;

	public constructor(canvas: HTMLCanvasElement) {
		this.width = canvas.width;
		this.data = canvas.getContext('2d').getImageData(0, 0, canvas.width, canvas.height).data;
	}

	public getPixelColor(x: number, y: number): Color {
		const index = (y * this.width + x) * 4;
		return new Color([
			this.data[index],
			this.data[index + 1],
			this.data[index + 2],
			this.data[index + 3],
		]);
	}
}

export default class Sprite {

	public name: string;
	
	private canvas = document.createElement('canvas');

	public async loadFromUrl(url: string): Promise<void> {
		const img = await downloadImage(url);
		this.setSize(img.width, img.height);
		this.ctx.drawImage(img, 0, 0, img.width, img.height);
	}

	public loadFromSpriteSheet(spriteSheet: HTMLImageElement, x: number, y: number, w: number, h: number) {
		this.setSize(w, h);
		this.ctx.drawImage(spriteSheet, x, y, w, h, 0, 0, w, h);
	}

	public setPixelColor(x: number, y: number, color: Color) {
		this.ctx.fillStyle = color.cssName;
		this.ctx.fillRect(x, y, 1, 1);
	}

	public getPixelColor(x: number, y: number): Color {
		const rgba = this.ctx.getImageData(x, y, 1, 1).data;
		return new Color([rgba[0], rgba[1], rgba[2], rgba[3]]);
	}

	public scale(factor: number) {
		const newCanvas = document.createElement('canvas');
		const newCtx = newCanvas.getContext('2d');
		newCanvas.width = Math.floor(this.width * factor);
		newCanvas.height = Math.floor(this.height * factor);
		setImageSmoothing(newCtx, false);
		newCtx.scale(factor, factor);
		newCtx.drawImage(this.canvas, 0, 0);
		newCtx.scale(1 / factor, 1 / factor);
		this.canvas = newCanvas;
	}

	public addMargins(top: number, right: number, bottom: number, left: number) {
		const newCanvas = document.createElement('canvas');
		newCanvas.width = this.width + right + left;
		newCanvas.height = this.height + top + bottom;
		newCanvas.getContext('2d').drawImage(this.canvas, left, top);
		this.canvas = newCanvas;
	}

	public map(from: Color[], to: Color[]) {
		const raster = new Raster(this.canvas);
		for (let x=0 ; x<this.width ; ++x) {
			for (let y=0 ; y<this.height ; ++y) {
				this.setPixelColor(x, y, to[from.indexOfNearest(raster.getPixelColor(x, y), Color.squaredDistance)]);
			}
		}
	}

	public displayInPlaceOf(domElement: Node, ...className: string[]): HTMLCanvasElement {
		if (this.name) this.canvas.title = this.name;
		this.canvas.classList.add(...className);
		domElement.parentNode.replaceChild(this.canvas, domElement);
		return this.canvas;
	}

	public displayInside(domElement: Node, ...className: string[]) {
		if (this.name) this.canvas.title = this.name;
		this.canvas.classList.add(...className);
		domElement.appendChild(this.canvas);
	}

	public onClick(listener: (this: HTMLCanvasElement, ev: MouseEvent) => any) {
		this.canvas.addEventListener('click', listener);
	}

	public getCopy(): Sprite {
		const sprite = new Sprite();
		sprite.canvas = document.createElement('canvas');
		sprite.canvas.width = this.canvas.width;
		sprite.canvas.height = this.canvas.height;
		sprite.name = this.name;
		sprite.ctx.drawImage(this.canvas, 0, 0);
		return sprite;
	}

	public supperpose(sprite: Sprite) {
		this.ctx.drawImage(sprite.canvas, 0, 0);
	}

	public get width() {
		return this.canvas.width;
	}

	public get height() {
		return this.canvas.height;
	}

	private get ctx() {
		return this.canvas.getContext('2d');
	}

	private setSize(width: number, height: number) {
		this.canvas.width = width;
		this.canvas.height = height;
	}
}
