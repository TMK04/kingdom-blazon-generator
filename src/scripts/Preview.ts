/* Copyright 2019, 2020 Clément Saccoccio */

/*
This file is part of Kingdom Blazon Generator.

Kingdom Blazon Generator is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

Kingdom Blazon Generator is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Kingdom Blazon Generator.  If not, see <https://www.gnu.org/licenses/>.
*/

import Color from './Color';
import Sprite from './Sprite';
import { onSelectedGameChanged } from './GameSelector';
import SpriteRegistry from './SpriteRegistry';

class Preview {
	private canvas = document.querySelector('#preview canvas');
	private textOutput = document.getElementById('text-output');
	private ignoreRefresh = false;
	private fieldColor: Color;
	private ordinaryModel: Sprite;
	private ordinaryColor: Color;
	private chargeModel: Sprite;
	private chargeColor: Color;

	public constructor() {
		onSelectedGameChanged(() => this.refresh());
	}

	public setFieldColor(color: Color) {
		this.fieldColor = color;
		this.refresh();
	}

	public setOrdinaryModel(model: Sprite) {
		this.ordinaryModel = model;
		this.refresh();
	}

	public setOrdinaryColor(color: Color) {
		this.ordinaryColor = color;
		this.refresh();
	}

	public setChargeModel(model: Sprite) {
		this.chargeModel = model;
		this.refresh();
	}

	public setChargeColor(color: Color){
		this.chargeColor = color;
		this.refresh();
	}

	public preventRefreshUntilForce() {
		this.ignoreRefresh = true;
	}

	public forceRefresh() {
		this.ignoreRefresh = false;
		this.refresh();
	}

	public refresh() {

		if (this.ignoreRefresh) return;

		const ordinaryModel = SpriteRegistry.ordinaries.getSubstituteIfNeeded(this.ordinaryModel);
		const chargeModel = SpriteRegistry.charges.getSubstituteIfNeeded(this.chargeModel);

		this.textOutput.textContent = `${this.fieldColor.name} ${ordinaryModel.name} ${this.ordinaryColor.name}`;
		let sprite = ordinaryModel.getCopy();

		if (chargeModel) {
			sprite.supperpose(chargeModel);
			this.textOutput.textContent += ` ${chargeModel.name} ${this.chargeColor.name}`;
		}

		sprite.map([
			Color.RED,
			Color.CYAN,
			Color.GREEN,
			Color.MAGENTA,
			Color.BLUE,
			Color.YELLOW,
			Color.TRANSPARENT,
		], [
			this.fieldColor,
			this.fieldColor.shadowed,
			this.ordinaryColor,
			this.ordinaryColor.shadowed,
			this.chargeColor,
			this.chargeColor.shadowed,
			Color.TRANSPARENT,
		]);

		sprite.scale(6);
		sprite.addMargins(57, 96, 66, 96);

		this.canvas = sprite.displayInPlaceOf(this.canvas);
	}
}

export default new Preview();
