/* Copyright 2019, 2020 Clément Saccoccio */

/*
	This file is part of Kingdom Blazon Generator.

	Kingdom Blazon Generator is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	any later version.

	Kingdom Blazon Generator is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Kingdom Blazon Generator.  If not, see <https://www.gnu.org/licenses/>.
*/

import { colors, unprefixedColors } from './Colors';
import Pannel from './Pannel';
import preview from './Preview';
import SpriteRegistry from './SpriteRegistry';

class FieldPannel extends Pannel {
	public constructor() {
		super(0);
		this.initColorPicker(colors, (color) => preview.setFieldColor(color));
	}
}

class OrdinaryPannel extends Pannel {
	public constructor() {
		super(1);
		this.initColorPicker(colors, (color) => preview.setOrdinaryColor(color));
		this.initSprites(SpriteRegistry.ordinaries, (model) => preview.setOrdinaryModel(model));
	}
}

class ChargePannel extends Pannel {
	public constructor() {
		super(2);
		this.initColorPicker(unprefixedColors, (color) => preview.setChargeColor(color));
		this.initSprites(SpriteRegistry.charges, (model) => preview.setChargeModel(model));
	}
}

let pannels: Pannel[];

export default function getPannels() {
	if (!pannels) {
		pannels = [new FieldPannel(), new OrdinaryPannel(), new ChargePannel()];
	}
	return pannels;
}
