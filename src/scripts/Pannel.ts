/* Copyright 2019, 2020 Clément Saccoccio */

/*
	This file is part of Kingdom Blazon Generator.

	Kingdom Blazon Generator is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	any later version.

	Kingdom Blazon Generator is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Kingdom Blazon Generator.  If not, see <https://www.gnu.org/licenses/>.
*/

import Color from './Color';
import ColorPicker from './ColorPicker';
import { onSelectedTabChanged } from './Tabs';
import Sprite from './Sprite';
import Game from './Game';
import { getSelectedGame } from './GameSelector';
import SpriteRegistry from './SpriteRegistry';
import { loadGlobalScopeExtensions } from './GlobalScopeExtensions';

loadGlobalScopeExtensions();

export default abstract class Pannel {

	private index: number;
	private element: HTMLElement;
	private colorPicker: ColorPicker;
	private selectRandomSprite: () => void;

	public constructor(index: number) {
		this.index = index;
		this.element = document.getElementsByClassName('pannel')[index] as HTMLElement;
		onSelectedTabChanged((_oldIndex, newIndex) => {
			this.element.style.display = newIndex === this.index ? '' : 'none';
		});
	}

	public randomize() {
		this.colorPicker?.selectRandom();
		this.selectRandomSprite?.call(this);
	}

	protected initColorPicker(colors: readonly Color[], onSelectedColorChanged: (color: Color) => void) {
		this.colorPicker = new ColorPicker(this.element.querySelector('.color-picker'), colors);
		this.colorPicker.onSelectedColorChanged((color) => onSelectedColorChanged(color));
	}

	protected initSprites(spriteRegistry: SpriteRegistry, onSelectedSpriteChanged: (sprite: Sprite) => void) {
		this.selectRandomSprite = () => {
			onSelectedSpriteChanged(spriteRegistry.getSprites(getSelectedGame()).randomElement());
		};
		const mountPoint = this.element.querySelector('.sprite-btns-container');
		for (const sprite of spriteRegistry.getSprites()) {
			const spriteInDOM = sprite.getCopy();
			spriteInDOM.map([
				Color.RED,
				Color.CYAN,
				Color.GREEN,
				Color.MAGENTA,
				Color.BLUE,
				Color.YELLOW,
				Color.TRANSPARENT,
			], [
				Color.BLACK,
				Color.BLACK,
				Color.BEIGE,
				Color.BEIGE,
				Color.BEIGE,
				Color.BEIGE,
				Color.TRANSPARENT,
			]);
			spriteInDOM.scale(3);
			spriteInDOM.displayInside(
				mountPoint,
				'sprite-btn',
				...spriteRegistry.getGamesWhereSpriteIsPresent(sprite).map((g) => g.id.toLowerCase())
			);
			spriteInDOM.onClick(() => onSelectedSpriteChanged(sprite));
		}
	}
}
