/* Copyright 2019, 2020 Clément Saccoccio */

/*
	This file is part of Kingdom Blazon Generator.

	Kingdom Blazon Generator is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	any later version.

	Kingdom Blazon Generator is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Kingdom Blazon Generator.  If not, see <https://www.gnu.org/licenses/>.
*/

import { setSelectedTabIndex } from './Tabs';
import randomize from './Randomizer';
import initPannels from './Pannels';

class GUI {
	public constructor() {
		initPannels();
		setSelectedTabIndex(1);
		randomize();
	}
}

let gui: GUI;

export function createGui() {
	if (!gui) {
		gui = new GUI();
	}
}
