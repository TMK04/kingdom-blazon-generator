/* Copyright 2019, 2020 Clément Saccoccio */

/*
	This file is part of Kingdom Blazon Generator.

	Kingdom Blazon Generator is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	any later version.

	Kingdom Blazon Generator is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Kingdom Blazon Generator.  If not, see <https://www.gnu.org/licenses/>.
*/

export default class Game {
	public static NEW_LAND = new Game('NL');
	public static TWO_CROWNS = new Game('TC');
	public static TWO_CROWNS_SHOGUN = new Game('TCS');
	public static TWO_CROWNS_DEAD_LANDS = new Game('TCDL');

	private static _all: Game[];

	public static get all(): readonly Game[] {
		return Game._all;
	}

	public readonly index: number;
	public readonly id: string;

	private constructor(id: string) {
		if (!Game._all) Game._all = [];
		this.index = Game._all.length;
		this.id = id;
		Game._all.push(this);
	}

	public hasNext(): boolean {
		return this.index < Game._all.length - 1;
	}

	public hasPrevious(): boolean {
		return this.index > 0;
	}

	public get next(): Game {
		return Game._all[this.index + 1];
	}

	public get previous(): Game {
		return Game._all[this.index - 1];
	}
}
