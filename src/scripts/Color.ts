/* Copyright 2019, 2020 Clément Saccoccio */

/*
	This file is part of Kingdom Blazon Generator.

	Kingdom Blazon Generator is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	any later version.

	Kingdom Blazon Generator is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Kingdom Blazon Generator.  If not, see <https://www.gnu.org/licenses/>.
*/

export default class Color {

	public static WHITE       = new Color([255, 255, 255]);
	public static BLACK       = new Color([  0,   0,   0]);
	public static RED         = new Color([255,   0,   0]);
	public static GREEN       = new Color([  0, 255,   0]);
	public static BLUE        = new Color([  0,   0, 255]);
	public static CYAN        = new Color([  0, 255, 255]);
	public static MAGENTA     = new Color([255,   0, 255]);
	public static YELLOW      = new Color([255, 255,   0]);
	public static TRANSPARENT = new Color([  0,   0,   0,  0]);
	public static BEIGE       = new Color([224, 191, 129]);

	public static squaredDistance(a: Color, b: Color): number {
		return (a._r - b._r) ** 2 + (a._g - b._g) ** 2 + (a._b - b._b) ** 2 + (a.a - b.a) ** 2;
	}

	public static compareLuminance(a: Color, b: Color): number {
		return a.luminance - b.luminance;
	}

	public static compareHue(a: Color, b: Color): number {
		return a.h - b.h;
	}

	private _name: string;
	private _r: number;
	private _g: number;
	private _b: number;
	private _a: number;
	private _h: number;
	private _s: number;
	private _v: number;
	private _luminance: number;
	private _cssName: string;
	private _shadowed: Color;

	public constructor(rgbOrRgba: number[], name?: string) {
		this._r = rgbOrRgba[0];
		this._g = rgbOrRgba[1];
		this._b = rgbOrRgba[2];
		if (rgbOrRgba.length > 3 && rgbOrRgba[3] !== 255) this._a = rgbOrRgba[3];
		if (name) this._name = name;
	}

	public get name(): string {
		return this._name;
	}

	public get r(): number {
		return this._r;
	}

	public get g(): number {
		return this._g;
	}

	public get b(): number {
		return this._b;
	}

	public get a(): number {
		return this._a ?? 255;
	}

	public get h(): number {
		if (this._h === undefined) this.computeHsv();
		return this._h;
	}

	public get s(): number {
		if (this._s === undefined) this.computeHsv();
		return this._s;
	}

	public get v(): number {
		if (this._v === undefined) this.computeHsv();
		return this._v;
	}

	public get luminance(): number {
		if (this._luminance === undefined) {
			this._luminance = Math.sqrt(
				0.299 * this._r**2
			  + 0.587 * this._g**2
			  + 0.114 * this._b**2);
		}
		return this._luminance;
	}

	public get cssName(): string {
		if (!this._cssName) {
			if (this.a === 255) {
				this._cssName = `rgb(${this._r},${this._g},${this._b})`;
			} else {
				this._cssName = `rgba(${this._r},${this._g},${this._b},${this._a})`;
			}
		}
		return this._cssName;
	}

	public get shadowed(): Color {
		if (!this._shadowed) {
			this._shadowed = new Color([
				Math.round(this._r * 205 / 255),
				Math.round(this._g * 205 / 255),
				Math.round(this._b * 205 / 255)
			]);
		}
		return this._shadowed;
	}

	private computeHsv() {
		const r = this._r / 255;
		const g = this._g / 255;
		const b = this._b / 255;
		const max = Math.max(r, g, b);
		const min = Math.min(r, g, b);
		const dif = max - min;

		switch (max) {
			case min:
				this._h = 0;
				break;
			case r:
				this._h = (g - b) / dif;
				break;
			case g:
				this._h = (b - r) / dif + 2;
				break;
			case b:
				this._h = (r - g) / dif + 4;
				break;
		}

		this._h *= 60;
		if (this._h < 0) this._h += 360;

		this._s = dif === 0 ? 0 : dif / max;
		this._v = max;
	}
}

